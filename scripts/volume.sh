#!/bin/bash

# You can call this script like this:
# $ ./volume.sh up
# $ ./volume.sh down
# $ ./volume.sh mute

function send_notification {
    icon_sound="audio-volume-high-symbolic.symbolic"
    icon_muted="audio-volume-muted-symbolic.symbolic"
    if [ $(pamixer --get-mute) = false ] ; then
        dunstify -i ${icon_muted} -u normal "mute"
    else
        # make the bar with the special character ─ (it's not dash -)
        # https://en.wikipedia.org/wiki/Box-drawing_character

        volume=$(pamixer --get-volume)
        bar=$(seq --separator="─" 0 "$((volume / 5))" | sed 's/[0-9]//g')

        dunstify -i ${icon_sound} -u normal "    $bar"
    fi
}

case ${1} in
    up)
        # increase volume
        pamixer --increase 5
        send_notification
        ;;
    down)
        # decrease volume
        pamixer --decrease 5
        send_notification
        ;;
    mute)
        # toggle mute
        pamixer -t
        send_notification
        ;;
esac
