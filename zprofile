# zprofile

# default programs
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="mupdf"
export FILE="ranger"

# memes
export ANSIBLE_NOCOWS=1

# home cleanup
export ZDOTDIR="$HOME/.config/zsh"
export LESSHISTFILE="-"
export WGETRC="$HOME/.config/wget/wgetrc"
export GNUPGHOME="$HOME/.config/gnupg"
export XAUTHORITY="$HOME/.local/share/Xauthority"
export VIMINIT='source ~/.config/nvim/vimrc'

# history
export HISTFILE=~/.cache/zsh/history
export HISTSIZE=SAVEHIST=10000
setopt sharehistory
setopt extendedhistory

# change keyboard layout based on hostname
[ $(< /etc/hostname) = "t420" ] && export XKB_DEFAULT_LAYOUT=be

# start river if on tty1
[ "$(tty)" = "/dev/tty1" ] && river

# start gnome if on tty2
[ "$(tty)" = "/dev/tty2" ] && gnome-shell --wayland
