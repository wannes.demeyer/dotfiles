" nvimrc

syntax on

filetype on
filetype plugin indent on
filetype plugin on

set nohlsearch
set hidden
set noswapfile
set nobackup
set undodir=~/.local/share/nvim/undodir
set undofile
set incsearch
set scrolloff=8
set signcolumn=yes
set number
set relativenumber
set expandtab ts=4 sw=4 ai
set cursorline
set cmdheight=2
set makeprg=gcc\ -o\ %<\ %

" plugins

call plug#begin('~/.config/vim/plugged')

Plug 'junegunn/goyo.vim'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'w0rp/ale'
Plug 'Yggdroot/indentLine'

" languages
Plug 'vim-latex/vim-latex'
Plug 'pearofducks/ansible-vim'
Plug 'rust-lang/rust.vim'

" code completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

" remaps

let mapleader = " "

map <leader>g :Goyo <Enter>
map <leader>n :NERDTreeToggle<CR>

" autocmds

fun! TrimWhiteSpace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup GROUP
    autocmd!
    autocmd BufWritePre * :call TrimWhiteSpace()
    autocmd FileType yml setlocal et ts=2 ai sw=2 sts=0
augroup END



let g:Tex_DefaultTargetFormat = 'pdf'

