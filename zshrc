# zshrc

autoload -Uz compinit
compinit

# vim keybinds
bindkey -v

# colors support
autoload -U colors colors

# prompt customization
PS1='%B%~%b '

# aliases
source "$HOME/.config/zsh/aliases"

# path
PATH=$PATH:~/.local/bin

# ranger fix
ranger() {
    if [ -z "$RANGER_LEVEL" ]; then
        /usr/bin/ranger "$@"
    else
        exit
    fi
}

# syntax highlighting
source /home/wannes/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# remove mode switching delay
KEYTIMEOUT=5

# change cursor shape depending on mode
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'

  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

_fix_cursor() {
   echo -ne '\e[5 q'
}

precmd_functions+=(_fix_cursor)
