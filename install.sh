#!/bin/sh
# install.sh

[ "$EUID" = 0 ] && echo "please don't run as root!" && exit 1

DOTFILES="${HOME}/repos/dotfiles"
SCRIPTS="${DOTFILES}/scripts"

echo "creating directories if they don't exist"
mkdir -p\
    ~/.config/river\
    ~/.config/sway\
    ~/.config/nvim\
    ~/.config/alacritty\
    ~/.config/mpd\
    ~/.config/wget\
    ~/.config/zsh/plugins\
    ~/.config/polybar\
    ~/.config/ranger && echo "created dirs!"

echo "installing vim-plug"
[ -f ~/.local/share/nvim/site/autoload/plug.vim ] || curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "installing zsh color highlighting"
[ -d ~/.config/zsh/plugins/zsh-syntax-highlighting ] || git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.config/zsh/plugins/zsh-syntax-highlighting

echo "linking files"
ln -sfv $DOTFILES/zprofile ~/.config/zsh/.zprofile
ln -sfv $DOTFILES/zshrc ~/.config/zsh/.zshrc
ln -sfv $DOTFILES/aliases ~/.config/zsh/aliases
ln -sfv $DOTFILES/vimrc ~/.config/nvim/vimrc
ln -sfv $DOTFILES/mpd.conf ~/.config/mpd/mpd.conf
ln -sfv $DOTFILES/rc.conf ~/.config/ranger/rc.conf
ln -sfv $DOTFILES/polybar.config ~/.config/polybar/config
ln -sfv $DOTFILES/polybar.launch.sh ~/.config/polybar/launch.sh
ln -sfv $DOTFILES/alacritty.yml ~/.config/alacritty/alacritty.yml

echo "enabling syncthing to start on login"
systemctl enable --user syncthing.service

echo "setting zsh env"
echo "export ZDOTDIR=/home/wannes/.config/zsh" | sudo tee /etc/zsh/zshenv

